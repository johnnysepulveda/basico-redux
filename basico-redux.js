const redux = require('redux');

const createStore = redux.createStore;

// State inicial
const stateInicial = {
    usuarios:[]
}

// Reducer
// Que define como el state va a cambiar de acuerdo a ciertas acciones
// toma como parametros el state y la accion
const reducerPrincipal = (state = stateInicial,action) => {

    if(action.type === 'AGREGAR_USUARIO'){
        return{
            ...state, // retornamos el state actual
            usuarios: action.nombre
        }
    }
    if(action.type === 'MOSTRAR_USUARIOS'){
        return{
            ...state
        }
    }

    return state;
}

// creamos el store
// esta función necesita tres parametros
// reducer, state y applymiddleware
// el reducer define como va a cambiar el state
const store = createStore(reducerPrincipal); // aqui es donde se almacena el state de la aplicacion
console.log( store.getState() );

// Suscribe o suscripcion
// es una funcion que escuicha los cambis, cada que una accion usa el dispatch, 
// cada vez que hay un cambio en el state, se ejecuta el suscribe
store.subscribe(() => {
    console.log('Algo cambio...', store.getState() );
})

// Dispatch: es la forma de cambiar el state
// esto viene a ser la accion que se llama en el reducer
//                   ACTION             ,       DATA
store.dispatch({ type: 'AGREGAR_USUARIO', nombre: 'Juan' });
store.dispatch({ type: 'MOSTRAR_USUARIOS'});

console.log( store.getState() );
# Redux #

Redux me permite manejar el state de las aplicaciones mas facíl cuando la aplicación es mediana grande.
Una caracteristica de redux es que me permite manejar los datos segun las acciones realizadas por el usuario.

### Caracteristicas ###

* Solo tiene un state principal. Se conoce como el store
* El state cambia de acuerdo a lo que sucede en la interfaz de usuario. Presionar un botón
* Solo ciertas funciones cambian el state: Los componentes dejaran de cambiar el state y seran funciones que se conocen como acciones que cambiaran el state 
* El usuario es el que utiliza estas funciones y cambia el state
* Solo se realiza un cambio a la vez

### Cuando utilizar Redux ###

Si la aplicación es pequeña, 20 componentes entonces deberiamos usar props o context. SI el proyecto es grande y es mantenido por varias personas, redux será de gran ayuda.

### Principios de redux ###

* Solo existe un store con todo el state de la aplicación
* Los componentes/vistas no modifican el state directamente, no se usa setState(), el state cambia por medio de funciones que se disparan hacia el store

### Terminología ###

* STORE: contiene el state (1 por aplicación)
* DISPATCH: Ejecuta una acción que actualizará el state
* ACTION: Objetos js, tienen un tipo y payload (datos)
* SUBSCRIBE: Similar a un event listener para el state, escucha por los cambios que pasan en el state.
* REDUCERS: Son funciones, saben lo que tienen que hacer cuando son llamados por las acciones y tienen un payload.

### como correr ###

* El proyecto se esta programando sobre node, entonces para correr: node<   > basico-redux.js